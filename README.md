
# khkSDL2

Just my silly 2D game framework based on SDL2(migrate to SDL3) library written in c.


## Dependencies

 - [SDL3](https://www.libsdl.org/)
 - [SDL3_image](https://github.com/libsdl-org/SDL_image)



## Demo
![khkSokoban](https://img.itch.zone/aW1nLzE4Mzg4ODUzLnBuZw==/original/bk2CaT.png)

https://kenhyokun.itch.io/khksokoban