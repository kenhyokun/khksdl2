/*
License under zlib license
Copyright (C) 2025 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "khkSDLx.h"

Node* create_node(const char *name, const char *tag)
{
	Node *node = malloc(sizeof(Node));

	if(name)
		node->name = name;
	else
		node->name = "Unamed Node";

	if(tag)
		node->tag = tag;
	else
		node->tag = "Untagged Node";

	node->parent = NULL;
	node->child = malloc(sizeof(Node*));
	node->child[0] = NULL;
	node->child_size = 0;
	// create_list(&node->components);
	node->data = NULL;
	node->transform = transform_0;
	node->rel_transform = transform_0;

	return node;
}

void node_add_child(Node *parent, Node *child)
{
	if(child->parent != parent)
	{
		size_t new_size = parent->child_size + 1;
		parent->child = realloc(parent->child, sizeof(Node*) * new_size);
		parent->child[parent->child_size] = child;		
		++parent->child_size;
		
		if(child->parent != NULL)		
			node_remove_child_node(child->parent, child);
		
		v2_sub(&child->rel_transform.position, child->transform.position, parent->transform.position);
		 		
		child->parent = parent;
	}
}

void node_pop_child(Node *parent)
{
	size_t new_size = parent->child_size - 1;
	size_t re_size = new_size;
	
	if(re_size <= 0) 
		re_size = 1;
	
	parent->child[new_size] = NULL;
	parent->child = realloc(parent->child, sizeof(Node*) * re_size);
	parent->child_size = new_size;
}

void node_remove_child(Node *parent, int index)
{
	if(index < parent->child_size - 1)
	{		
		parent->child[index]->parent = NULL;
				
		for(int i = index; i < parent->child_size; ++i)
		{
			parent->child[i] = parent->child[i + 1];
		}
	}	
	
	node_pop_child(parent);
}

void node_free_child(Node *parent, int index)
{
	if(index < parent->child_size - 1)
	{		
		parent->child[index]->parent = NULL;
		free(parent->child[index]);
				
		for(int i = index; i < parent->child_size; ++i)
		{
			parent->child[i] = parent->child[i + 1];
		}
	}	
	
	node_pop_child(parent);
}

void node_swap_child_index(Node *parent, int index_i, int index_j)
{
	Node *temp = parent->child[index_i];
	parent->child[index_i] = parent->child[index_j];
	parent->child[index_j] = temp;
}

int get_child_index(Node *parent, Node *child)
{
	for(int i = 0; i < parent->child_size; ++i)
	{
		if(parent->child[i] == child)
		{
			return i;
		}
	}
	
	return -1;
}

void node_remove_child_node(Node *parent, Node *child)
{
	int index = get_child_index(parent, child);
	node_remove_child(parent, index);
}

void node_free_child_node(Node *parent, Node *child)
{
	int index = get_child_index(parent, child);
	node_free_child(parent, index);
}

void node_set_position(Node *node, v2 position)
{		
	v2 r;
	v2_sub(&r, position, node->transform.position);		
	node->transform.position = position;
	
	if(node->parent)
	{
		v2_sub(&node->rel_transform.position, node->transform.position, node->parent->transform.position);
	}
	
	if(node->child_size > 0)
	{			
		for(int i = 0; i < node->child_size; ++i)
		{
			Node *child = node->child[i];
			v2 child_position;
			v2_add(&child_position, child->transform.position, r);
			node_set_position(child, child_position);
		}
	}
}

void node_set_position_x(Node *node, float x)
{
	float r = x - node->transform.position.x;	
	node->transform.position.x = x;
	
	if(node->parent)
	{
		node->rel_transform.position.x = node->transform.position.x - node->parent->transform.position.x;
	}
	
	if(node->child_size > 0)
	{			
		for(int i = 0; i < node->child_size; ++i)
		{
			Node *child = node->child[i];
			float child_x = child->transform.position.x + r;				
			node_set_position_x(child, child_x);			
		}
	}
}

void node_set_position_y(Node *node, float y)
{
	float r = y - node->transform.position.y;	
	node->transform.position.y = y;
	
	if(node->parent)
	{
		node->rel_transform.position.y = node->transform.position.y - node->parent->transform.position.y;
	}
	
	if(node->child_size > 0)
	{			
		for(int i = 0; i < node->child_size; ++i){
			Node *child = node->child[i];
			float child_y = child->transform.position.y + r;
			node_set_position_y(child, child_y);
			child->rel_transform.position.y = child->transform.position.y - node->transform.position.y;
		}
	}
}

void node_set_rel_position(Node *node, v2 rel_position)
{
	v2 position;
	v2_add(&position, node->parent->transform.position, rel_position);
	node_set_position(node, position);
}

void node_set_rel_position_x(Node *node, float rel_x)
{
	float x = node->parent->transform.position.x + rel_x;
	node_set_position_x(node, x);
}

void node_set_rel_position_y(Node *node, float rel_y)
{
	float y = node->parent->transform.position.y + rel_y;
	node_set_position_y(node, y);
}

void node_move_towards(Node *node, float degree, float speed)
{	
	v2 move_position = node->transform.position;
	v2_move_towards(&move_position, degree, speed);
	node_set_position(node, move_position);
}

void node_move_towards_v2(Node *node, v2 target, float speed)
{
	v2 move_position = node->transform.position;
	v2_move_towards_v2(&move_position, target, speed);
	node_set_position(node, move_position);
}

void node_move_towards_v2_r(Node *node, v2 target, float r, float speed)
{
	v2 move_position = node->transform.position;
	v2_move_towards_v2_r(&move_position, target, r, speed);
	node_set_position(node, move_position);
}

void node_move_towards_v2_d(Node *node, v2 target, float speed, float *get_distance)
{
	v2 move_position = node->transform.position;
	v2_move_towards_v2_d(&move_position, target, speed, get_distance);
	node_set_position(node, move_position);
}

void node_move_towards_v2_r_d(Node *node, v2 target, float r, float speed, float *get_distance)
{
	v2 move_position = node->transform.position;
	v2_move_towards_v2_r_d(&move_position, target, r, speed, get_distance);
	node_set_position(node, move_position);
}

void node_move_towards_v2_cb(Node *node, v2 target, float speed, void(*end_callback)())
{
	float distance;
	v2 move_position = node->transform.position;
	v2_move_towards_v2_d(&move_position, target, speed, &distance);
	node_set_position(node, move_position);
	
	if(distance == 0.0f)	
		(*end_callback)();	
}

void node_move_towards_v2_r_cb(Node *node, v2 target, float r, float speed, void(*end_callback)())
{
	float distance;
	v2 move_position = node->transform.position;
	v2_move_towards_v2_r_d(&move_position, target, r, speed, &distance);
	node_set_position(node, move_position);
	
	if(distance == 0.0f)	
		(*end_callback)();
}

void node_move_towards_node(Node *node, Node *target_node, float speed)
{
	node_move_towards_v2(node, target_node->transform.position, speed);
}