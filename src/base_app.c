/*
License under zlib license
Copyright (C) 2025 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "khkSDLx.h"

Sys sys;

BASE_APP SDL_Rect screen_viewport;
BASE_APP SDL_Rect world_viewport;
BASE_APP SDL_Event event;

App *create_app(const char *title, int window_width, int window_height, int target_fps)
{
	App *app = malloc(sizeof(App));
	app->window_width = window_width;
	app->window_height = window_height;
	app->mouse_position = v2i_0;
	app->is_running = true;
	
	sys.target_fps = target_fps;
	sys.dt = 1000.0f / (float)target_fps;
	sys.ticks = 0.0f;

	// app->window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window_width, window_height, SDL_WINDOW_RESIZABLE);

	SDL_PropertiesID props = SDL_CreateProperties();
    SDL_SetStringProperty(props, SDL_PROP_WINDOW_CREATE_TITLE_STRING, title);
    SDL_SetNumberProperty(props, SDL_PROP_WINDOW_CREATE_X_NUMBER, SDL_WINDOWPOS_CENTERED);
    SDL_SetNumberProperty(props, SDL_PROP_WINDOW_CREATE_Y_NUMBER, SDL_WINDOWPOS_CENTERED);
    SDL_SetNumberProperty(props, SDL_PROP_WINDOW_CREATE_WIDTH_NUMBER, window_width);
    SDL_SetNumberProperty(props, SDL_PROP_WINDOW_CREATE_HEIGHT_NUMBER, window_height);
    SDL_SetNumberProperty(props, SDL_PROP_WINDOW_CREATE_FLAGS_NUMBER, SDL_WINDOW_RESIZABLE);
    app->window = SDL_CreateWindowWithProperties(props);
    SDL_DestroyProperties(props);

    if (app->window) {
		printf("Window app has been created!\n");
    }

	app->renderer = SDL_CreateRenderer(app->window, NULL);
	
	SDL_SetWindowMinimumSize(app->window, window_width, window_height);
	// SDL_SetRenderLogicalPresentation(app->renderer, window_width, window_height, SDL_LOGICAL_PRESENTATION_INTEGER_SCALE); 
	SDL_SetRenderLogicalPresentation(app->renderer, window_width, window_height, SDL_LOGICAL_PRESENTATION_LETTERBOX);

	screen_viewport = (SDL_Rect){0, 0, window_width, window_height};
	world_viewport = (SDL_Rect){0, 0, window_width, window_height};	

	app->on_init = NULL;
	app->on_update = NULL;
	app->on_render = NULL;
	app->on_key_up = NULL;
	app->on_key_down = NULL;
	app->on_mouse_up = NULL;
	app->on_mouse_down = NULL;
	app->on_destroy = NULL;
	
	srand((unsigned int)time(NULL));

	return app;
}

void run_app(App *app)
{
	if(app->on_init) app->on_init(app);

	int total_frames = 0;

	while(app->is_running)
	{
		while(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
				case SDL_EVENT_QUIT:
					app->is_running = false;
					break;
				case SDL_EVENT_MOUSE_MOTION:
					app->mouse_position = (v2i){event.motion.x, event.motion.y};
					break;
				case SDL_EVENT_MOUSE_BUTTON_DOWN:
					if(app->on_mouse_down) app->on_mouse_down(app, event.button);
					break;
				case SDL_EVENT_MOUSE_BUTTON_UP:
					if(app->on_mouse_up) app->on_mouse_up(app, event.button);
					break;
				case SDL_EVENT_KEY_DOWN:
					if(app->on_key_down) app->on_key_down(app, event.key.key);
					break;
				case SDL_EVENT_KEY_UP:
					if(app->on_key_up) app->on_key_up(app, event.key.key);
					break;
			}
		}		
				
		sys.ticks += sys.dt;

		// update
		if(app->on_update) 
			app->on_update(app);

		// render
		SDL_SetRenderDrawColor(app->renderer, 0, 0, 0, 255); // letter box color
		SDL_RenderClear(app->renderer);

		if(app->on_render) 
			app->on_render(app);

		SDL_RenderPresent(app->renderer);
		
		++total_frames;
				
		SDL_Delay(sys.dt);
		
		if(sys.ticks >= 1000.0f)
		{
			// printf("ticks:%d, frames:%d\n", app->sys.ticks, app->sys.frames); // FIXME: not give right fps counter when game slow down		
			printf("ticks:%f, target fps:%d, total frames:%d\n", sys.ticks, sys.target_fps, total_frames);
			total_frames = 0;			
			sys.ticks = 0.0f;
		}
	}

	if(app->on_destroy) app->on_destroy(app);
	SDL_DestroyWindow(app->window);
	free(app);
	SDL_Quit();
}