/*
License under zlib license
Copyright (C) 2025 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "khkSDLx.h"

BASE_GRAPHICS
SDL_FRect get_sub_rect(int index, int column, int width, int height)
{
  // (i * column) + j = frame at index

  int i = index / column;
  int j = index - (i * column);
  SDL_FRect sub_rect;
  sub_rect.x = j * width;
  sub_rect.y = i * height;
  sub_rect.w = width;
  sub_rect.h = height;
  return sub_rect;
}

Surface* load_surface(const char *src_file)
{
	Surface *surface = IMG_Load(src_file);

	if(!surface)
	{
		printf("file not found!:%s\n", src_file);
		exit(1);
	}
	else
	{
		printf("load:%s\n", src_file);
	}

	return surface;
}

Texture* create_texture(SDL_Renderer *renderer, Surface *surface)
{
	Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_SetTextureScaleMode(texture, SDL_SCALEMODE_NEAREST);
	SDL_DestroySurface(surface);
	return texture;
}

Texture* load_texture(SDL_Renderer *renderer, const char *src_file)
{
	Surface *surface = load_surface(src_file);
	return create_texture(renderer, surface);
}

void render_texture(SDL_Renderer *renderer, Texture *texture, float pos_x, float pos_y)
{
	float texture_width;
	float texture_height;
	SDL_GetTextureSize(texture, &texture_width, &texture_height);
	SDL_FRect dst_rect = {pos_x, pos_y, texture_width, texture_height};
	SDL_RenderTextureRotated(renderer, texture, NULL, &dst_rect, 0, NULL, SDL_FLIP_NONE);
}

void render_texture_v2(SDL_Renderer *renderer, Texture *texture, v2 position)
{
		render_texture(renderer, texture, position.x, position.y);
}

void render_texture_ex(SDL_Renderer *renderer, Texture *texture, float pos_x, float pos_y, double angle, SDL_FPoint *center, SDL_FlipMode flip)
{	
	float texture_width;
	float texture_height;
	SDL_GetTextureSize(texture, &texture_width, &texture_height);
	SDL_FRect dst_rect = {pos_x, pos_y, texture_width, texture_height};
	SDL_RenderTextureRotated(renderer, texture, NULL, &dst_rect, angle, center, flip);
}

void render_texture_ex_v2(SDL_Renderer *renderer, Texture *texture, v2 position, double angle, SDL_FPoint *center, SDL_FlipMode flip)
{	
	render_texture_ex(renderer, texture, position.x, position.y, angle, center, flip);
}

void render_sub_texture(SDL_Renderer *renderer, Texture *texture, SDL_Rect *src_rect, float pos_x, float pos_y)
{
	SDL_FRect dst_rect = {pos_x, pos_y, src_rect->w, src_rect->h};
	SDL_RenderTextureRotated(renderer, texture, NULL, &dst_rect, 0, NULL, SDL_FLIP_NONE);
}

Sprite *create_sprite(SDL_Renderer *renderer, Texture *texture)
{
	Sprite *sprite = malloc(sizeof(Sprite));
	sprite->texture = texture;
	SDL_GetTextureSize(sprite->texture, &sprite->width, &sprite->height);
	sprite->pivot = (SDL_FPoint){sprite->width / 2, sprite->height / 2};
	sprite->flip = SDL_FLIP_NONE;
	return sprite;
}

Sprite *load_sprite(SDL_Renderer *renderer, const char *src_file)
{
	Texture *texture = load_texture(renderer, src_file);
	return create_sprite(renderer, texture);
}

void render_sub_sprite(SDL_Renderer *renderer, Sprite *sprite, float pos_x, float pos_y, SDL_FRect *src_rect)
{
	render_sub_sprite_v2(renderer, sprite, (v2){pos_x, pos_y}, (v2){1.0f, 1.0f}, 0.0f, src_rect, NULL);
}

void render_sub_sprite_v2(SDL_Renderer *renderer, Sprite *sprite, v2 position, v2 scale, double angle, SDL_FRect *src_rect, SDL_FPoint *pivot)
{
	SDL_FRect dst_rect;
	float scale_width = 0.0f;
	float scale_height = 0.0f;
	SDL_FPoint pv = {sprite->pivot.x, sprite->pivot.y};
	
	if(src_rect)
	{	
		if(!pivot)
			pv = (SDL_FPoint){src_rect->w / 2, src_rect->h / 2};
		else
			pv = *pivot;

		scale_width = src_rect->w * scale.x;
		scale_height = src_rect->h * scale.y;
		dst_rect.x = position.x - pv.x;
		dst_rect.y = position.y - pv.y;
		dst_rect.w = scale_width;
		dst_rect.h = scale_height;
	}
	else
	{
		scale_width = sprite->width * scale.x;
		scale_height = sprite->height * scale.y;
		dst_rect.x = position.x - sprite->pivot.x;
		dst_rect.y = position.y - sprite->pivot.y;
		dst_rect.w = scale_width;
		dst_rect.h = scale_height;
	}

	SDL_RenderTextureRotated(
			renderer, 
			sprite->texture, 
			src_rect,
			&dst_rect, 
			angle, 
			&pv,
			sprite->flip);
}

void render_sub_transform(SDL_Renderer *renderer, Sprite *sprite, Transform *transform, SDL_FRect *src_rect)
{
}

void render_sprite(SDL_Renderer *renderer, Sprite *sprite, float pos_x, float pos_y)
{
	render_sprite_v2(renderer, sprite, (v2){pos_x, pos_y}, (v2){1.0f, 1.0f}, 0.0f);
}

void render_sprite_v2(SDL_Renderer *renderer, Sprite *sprite, v2 position, v2 scale, double angle)
{
	render_sub_sprite_v2(renderer, sprite, position, scale, angle, NULL, NULL);
}

void render_sprite_transform(SDL_Renderer *renderer, Sprite *sprite, Transform *transform)
{
	render_sprite_v2(renderer, sprite, transform->position, transform->scale, transform->angle);
}

void destroy_sprite(Sprite *sprite)
{
	SDL_DestroyTexture(sprite->texture);	
	free(sprite);
	sprite = NULL;
}

SpriteAnimator* create_sprite_animator(Sprite *sprite, v2i frame_size, int target_fps)
{
	SpriteAnimator *sprite_animator = malloc(sizeof(SpriteAnimator));
	sprite_animator->sprite = sprite;
	sprite_animator->pivot = (SDL_FPoint){frame_size.x / 2, frame_size.y / 2};
	sprite_animator->target_fps = target_fps;
	sprite_animator->curr_frame_index = 0;
	sprite_animator->frame.size = frame_size;
	sprite_animator->frame.grid_size = (v2i){sprite->width / frame_size.x, sprite->height / frame_size.y};
	
	float fps = sys.target_fps / target_fps;
	sprite_animator->frame.dt = fps * sys.dt;

	sprite_animator->frame.next_frame_ms = SDL_GetTicks() + sprite_animator->frame.dt;
	sprite_animator->frame.src_rect = (SDL_FRect){0, 0, frame_size.x, frame_size.y};
	return sprite_animator;
}

void run_sprite_animator(SpriteAnimator *sprite_animator, int *frames, int frame_length)
{
	if(SDL_GetTicks() >= sprite_animator->frame.next_frame_ms)
	{
		if(sprite_animator->curr_frame_index < frame_length - 1)
			++sprite_animator->curr_frame_index;
		else
			sprite_animator->curr_frame_index = 0;

		sprite_animator->frame.src_rect = 
			get_sub_rect(frames[sprite_animator->curr_frame_index], 
				sprite_animator->frame.grid_size.x, 
				sprite_animator->frame.size.x, 
				sprite_animator->frame.size.y);	

		sprite_animator->frame.next_frame_ms = SDL_GetTicks() + sprite_animator->frame.dt;
	}	
}

void render_sprite_animator(SDL_Renderer *renderer, SpriteAnimator *sprite_animator, float pos_x, float pos_y)
{
	render_sub_sprite_v2(renderer, sprite_animator->sprite, (v2){pos_x, pos_y}, (v2){1.0f, 1.0f}, 0.0f, &sprite_animator->frame.src_rect, &sprite_animator->pivot);
}

void render_sprite_animator_transform(SDL_Renderer *renderer, SpriteAnimator *sprite_animator, Transform *transform)
{
	render_sub_sprite_v2(renderer, sprite_animator->sprite, transform->position, transform->scale, transform->angle, &sprite_animator->frame.src_rect, &sprite_animator->pivot);
}

void render_circle(SDL_Renderer *renderer, Circle *circle, SDL_Color color, int filled)
{
	SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
	
	switch(filled)
	{
		case 0:
		{
			int diameter = circle->radius * 2;
			int x = (circle->radius - 1);
			int y = 0;
			int dx = 1;
			int dy = 1;
			int err = dx - diameter;

			while(x >= y)
			{
				SDL_RenderPoint(renderer, circle->position.x + x, circle->position.y - y);
				SDL_RenderPoint(renderer, circle->position.x + x, circle->position.y + y);
				SDL_RenderPoint(renderer, circle->position.x - x, circle->position.y - y);
				SDL_RenderPoint(renderer, circle->position.x - x, circle->position.y + y);
				SDL_RenderPoint(renderer, circle->position.x + y, circle->position.y - x);
				SDL_RenderPoint(renderer, circle->position.x + y, circle->position.y + x);
				SDL_RenderPoint(renderer, circle->position.x - y, circle->position.y - x);
				SDL_RenderPoint(renderer, circle->position.x - y, circle->position.y + x);

				if(err <= 0)
				{
					++y;
					err += dy;
					dy += 2;
				}

				if(err > 0)
				{
					--x;
					dx += 2;
					err += dx - diameter;
				}
			}
		}
		break;
		
		case 1:
		{
			int x = circle->radius;
			int y = 0;
			int err = 1 - x;

			while(x >= y)
			{
				int start_x = -x + circle->position.x;
				int end_x = x + circle->position.x;
				
				SDL_RenderLine(renderer, start_x, y + circle->position.y, end_x, y + circle->position.y);			
				
				if(y != 0)
				{
					SDL_RenderLine(renderer, start_x, -y + circle->position.y, end_x, -y + circle->position.y);
				}

				y++;
				
				if(err < 0)
				{
					err += 2 * y + 1;
				}
				else 
				{		
					if(x >= y)
					{
						start_x = -y + 1 + circle->position.x;
						end_x = y - 1 + circle->position.x;
						SDL_RenderLine(renderer, start_x, x + circle->position.y, end_x, x + circle->position.y);		
						SDL_RenderLine(renderer, start_x, -x + circle->position.y, end_x, -x + circle->position.y);		
					}
					x--;
					err += 2 * (y - x + 1);
				}

			}
		}
		break;
	}
	
}
