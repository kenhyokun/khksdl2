/*
License under zlib license
Copyright (C) 2025 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "khkSDLx.h"

void create_list(List *list)
{
	list->t = malloc(sizeof(void*));
	list->size = 0;
}

void list_push_back(List *list, void *element)
{
	size_t new_size = list->size + 1;
	list->t = realloc(list->t, sizeof(void*) * new_size);
	list->t[list->size] = element;
	list->size = new_size;
}

void list_pop_back(List *list)
{
	size_t new_size = list->size - 1;	
	size_t re_size = new_size;
	
	if(re_size <= 0) 
		re_size = 1;
	
	list->t[new_size] = NULL;
	list->t = realloc(list->t, sizeof(void*) * re_size);
	list->size = new_size;
}

void erase_list_element(List *list, int index)
{
	if(index < list->size - 1)
	{
		for(int i = index; i < list->size; ++i)
		{
			list->t[i] = list->t[i + 1];
		}		
	}	
	
	list_pop_back(list);
}

void erase_list_element_element(List* list, void *element)
{
	
}

void swap_list_element(List *list, int index_a, int index_b)
{
	void *temp = list->t[index_a];
	list->t[index_a] = list->t[index_b];
	list->t[index_b] = temp;
}

void clear_list(List *list)
{
	free(list->t);
	list->size = 0;
}

