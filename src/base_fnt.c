/*
License under zlib license
Copyright (C) 2025 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "khkSDLx.h"

#define SECTION_HEAD -1
#define SECTION_CHAR 0
#define SECTION_KERNING 1

Fnt* load_fnt(SDL_Renderer *renderer, const char * src_file)
{
	int section_state = SECTION_HEAD;

	Fnt *fnt = malloc(sizeof(Fnt));
	fnt->glyph = NULL;
	
	FILE * file;
    char line[256];

    file = fopen(src_file, "r");
    if (!file)
	{
		printf("file not found!:%s\n", src_file);
		exit(0);
	}        

	List head_token_list;
	create_list(&head_token_list);
	
	List char_token_list;
	create_list(&char_token_list);

	while (fgets(line, sizeof(line), file)) 
	{
			if(strstr(line, "chars count") != NULL)
			{
				section_state = SECTION_CHAR;				
			}
			else if(strstr(line, "kernings count") != NULL)
			{
				section_state = SECTION_KERNING;
			}
									
			switch(section_state)
			{
				case SECTION_HEAD:				
					char *token = strtok(line, " ");					
					while (token != NULL)
					{						
						char *str = malloc(sizeof(char) * (strlen(token) + 1));
						strcpy(str, token);						
						list_push_back(&head_token_list, (void*)str);						
						token = strtok(NULL, " ");
					}																					
				break;
				
				case SECTION_CHAR:	
					char *token_char = strtok(line, " ");					
					while (token_char != NULL)
					{												
						char *str_char = malloc(sizeof(char) * (strlen(token_char) + 1));
						strcpy(str_char, token_char);						
						list_push_back(&char_token_list, (void*)str_char);																	
						
						token_char = strtok(NULL, " ");
					}		
				break;
				
				case SECTION_KERNING:
				break;
			}
	}

	fclose(file);
	
	for(int i = 0; i < head_token_list.size; ++i)
	{			
			List token_split;
			create_list(&token_split);
			
			char *token = strtok(head_token_list.t[i], "=");							
			while (token != NULL)
			{								
				char *str_char = malloc(sizeof(char) * (strlen(token) + 1));
				strcpy(str_char, token);						
				list_push_back(&token_split, (void*)str_char);						
				
				token = strtok(NULL, "=");
			}		
			
			if(strcmp(token_split.t[0], "size") == 0)
			{
				fnt->size = atoi(token_split.t[1]);
			}
			else if(strcmp(token_split.t[0], "bold") == 0)
			{
				fnt->bold = atoi(token_split.t[1]);
			}
			else if(strcmp(token_split.t[0], "italic") == 0)
			{
				fnt->italic = atoi(token_split.t[1]);
			}
			else if(strcmp(token_split.t[0], "file") == 0)
			{				
				fnt->image_src = token_split.t[1];
				fnt->image_src[strlen(token_split.t[1]) - 2] = '\0';
				memmove(fnt->image_src, fnt->image_src + 1, strlen(fnt->image_src));				
			}		
			
			clear_list(&token_split);
	}
	
	int glyph_index = 0;
	for(int i = 0; i < char_token_list.size; ++i)
	{						
		List token_split;
		create_list(&token_split);
		
		char *token = strtok(char_token_list.t[i], "=");
		while (token != NULL)
		{					
			char *str_char = malloc(sizeof(char) * (strlen(token) + 1));
			strcpy(str_char, token);						
			list_push_back(&token_split, (void*)str_char);					
			token = strtok(NULL, "=");
		}	
		
		if(strcmp(token_split.t[0], "count") == 0)
		{					
			fnt->char_count = atoi(token_split.t[1]);
			fnt->glyph = malloc(sizeof(Glyph*) * fnt->char_count);
		}
		else if(strcmp(token_split.t[0], "id") == 0)
		{
			// start of glyph data
			
			fnt->glyph[glyph_index] = malloc(sizeof(Glyph));
			fnt->glyph[glyph_index]->id = atoi(token_split.t[1]);
		}
		else if(strcmp(token_split.t[0], "x") == 0)
		{
			fnt->glyph[glyph_index]->x = atoi(token_split.t[1]);			
		}	
		else if(strcmp(token_split.t[0], "y") == 0)
		{
			fnt->glyph[glyph_index]->y = atoi(token_split.t[1]);
		}
		else if(strcmp(token_split.t[0], "width") == 0)
		{
			fnt->glyph[glyph_index]->width = atoi(token_split.t[1]);
		}	
		else if(strcmp(token_split.t[0], "height") == 0)
		{
			fnt->glyph[glyph_index]->height = atoi(token_split.t[1]);
		}	
		else if(strcmp(token_split.t[0], "xoffset") == 0)
		{
			fnt->glyph[glyph_index]->xoffset = atoi(token_split.t[1]);
		}
		else if(strcmp(token_split.t[0], "yoffset") == 0)
		{
			fnt->glyph[glyph_index]->yoffset = atoi(token_split.t[1]);
		}
		else if(strcmp(token_split.t[0], "xadvance") == 0)
		{
			fnt->glyph[glyph_index]->xadvance = atoi(token_split.t[1]);
		}
		else if(strcmp(token_split.t[0], "page") == 0)
		{
			fnt->glyph[glyph_index]->page = atoi(token_split.t[1]);
		}
		else if(strcmp(token_split.t[0], "chnl") == 0)
		{
			// end of glyph data
			
			fnt->glyph[glyph_index]->chnl = atoi(token_split.t[1]);
			++glyph_index;
		}			

		clear_list(&token_split);		
	}
	
	clear_list(&head_token_list);
	clear_list(&char_token_list);
	
	const char *fnt_dir_src = "./res/fnt/"; // TODO: get fnt file directory?
	char *image_src;
	strcpy(image_src, fnt_dir_src);
	strcat(image_src, fnt->image_src);
	fnt->texture = load_texture(renderer, image_src);
	
	printf("load:%s\n", src_file);
	
	return fnt;
}

FntText* create_fnt_text(const char *text, Fnt *fnt)
{
	FntText *fnt_text = malloc(sizeof(FntText));
	fnt_text->fnt = fnt;
	fnt_text->width = 0;
	fnt_text->height = 0;
	fnt_text->pivot = (SDL_Point){0, 0};

	fnt_text->glyph_index_arr = malloc(sizeof(int));
	set_fnt_text(fnt_text, text);	
	
	return fnt_text;
}

void set_fnt_text(FntText *fnt_text, const char *text)
{

	fnt_text->length = strlen(text);
	fnt_text->glyph_index_arr = realloc(fnt_text->glyph_index_arr, sizeof(int) * fnt_text->length);

	for(int i = 0; i < fnt_text->length; ++i)
	{
			int id = text[i];			
			fnt_text->glyph_index_arr[i] = id - fnt_text->fnt->glyph[0]->id;
			fnt_text->width += fnt_text->fnt->glyph[i]->width;
	}

	fnt_text->height = fnt_text->fnt->glyph[0]->height + fnt_text->fnt->glyph[0]->yoffset; // space character height
}

void render_fnt_text(SDL_Renderer *renderer, FntText *fnt_text, float pos_x, float pos_y)
{
	int prev_width = 0;
	for(int i = 0; i < fnt_text->length; ++i)
	{
		SDL_Rect src_rect = {
			fnt_text->fnt->glyph[fnt_text->glyph_index_arr[i]]->x, 
			fnt_text->fnt->glyph[fnt_text->glyph_index_arr[i]]->y, 
			fnt_text->fnt->glyph[fnt_text->glyph_index_arr[i]]->width, 
			fnt_text->fnt->glyph[fnt_text->glyph_index_arr[i]]->height
		};		
		
		// int offset_x = fnt_text->fnt->glyph[fnt_text->glyph_index_arr[i]]->xoffset;
		int offset_y = fnt_text->fnt->glyph[fnt_text->glyph_index_arr[i]]->yoffset;

		if(i > 0)
			prev_width += fnt_text->fnt->glyph[fnt_text->glyph_index_arr[i - 1]]->width;
		
		render_sub_texture(
			renderer, 
			fnt_text->fnt->texture, 
			&src_rect, 
			pos_x - fnt_text->pivot.x + prev_width, 
			pos_y - fnt_text->pivot.x + offset_y
		);
	}
}
