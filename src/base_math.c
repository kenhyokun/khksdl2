/*
License under zlib license
Copyright (C) 2025 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "khkSDLx.h"

double to_radian(float degree)
{
	return degree * (PI / 180.0f);
}

double to_degree(float radian)
{
	return radian * (180.0f / PI);
}

int get_digit(int num)
{
	return floor(log10(abs(num))) + 1;
}

v2 to_v2(v2i v)
{
		return (v2){(float)v.x, (float)v.y};
}

void v2_sub(v2 *dst, v2 a, v2 b)
{
	dst->x = a.x - b.x;
	dst->y = a.y - b.y;
}

void v2_add(v2 *dst, v2 a, v2 b)
{
	dst->x = a.x + b.x;
	dst->y = a.y + b.y;
}

float v2_magnitude(v2 v2)
{
	return sqrt((v2.x * v2.x) + (v2.y * v2.y));
}

float v2_distance(v2 a, v2 b)
{
	v2 d;
	v2_sub(&d, b, a);
	return v2_magnitude(d);
}

float v2_azimuth(v2 a, v2 b)
{
	v2 r;
	v2_sub(&r, b, a);
	float alpha = atan(r.x / r.y);
	
	if(isnan(alpha))
		return 0;

	bool is_x_positive = (r.x >= 0);
	bool is_x_negative = (r.x < 0);
	bool is_y_positive = (r.y >= 0);
	bool is_y_negative = (r.y < 0);

	if(is_x_positive && is_y_positive)
	{
		return 90.0 + (90.0 - to_degree(alpha));
	}
	else if(is_x_positive && is_y_negative)
	{
		return -to_degree(alpha);
	}
	else if(is_x_negative && is_y_negative)
	{
		return 270.0 + (90.0 - to_degree(alpha));
	}
	else if(is_x_negative && is_y_positive)
	{
		return 180.0 - to_degree(alpha);
	}

	return 0;
}

v2 v2_parametric(float degree, float r)
{
	double norm = degree - 90.0f;
	double param_x = cos(to_radian(norm)) * (double)r;
	double param_y = sin(to_radian(norm)) * (double)r;
	return (v2){param_x, param_y};
}

void v2_move_towards(v2 *dst, float degree, float speed)
{
	v2 parametric = v2_parametric(degree, speed);
	v2_add(dst, *dst, parametric);
}

void v2_move_towards_v2(v2 *dst, v2 target, float speed)
{
	float angle = v2_azimuth(*dst, target);
	float distance = v2_distance(*dst, target);
	
	if(distance - speed > 0.0f)
	{
		v2_move_towards(dst, angle, speed);
	}
	else
	{
		float r = speed + (distance - speed);
		v2_move_towards(dst, angle, r);
	}
}

void v2_move_towards_v2_r(v2 *dst, v2 target, float r, float speed)
{
	float angle = v2_azimuth(*dst, target);
	v2 target_r;
	v2_add(&target_r, target, v2_parametric(angle, r));

	// NOTE: recalculate azimuth.	
	v2_move_towards_v2(dst, target_r, speed);
}

void v2_move_towards_v2_cb(v2 *dst, v2 target, float speed, void(*end_callback)())
{
	float angle = v2_azimuth(*dst, target);
	float distance = v2_distance(*dst, target);
	
	if(distance - speed > 0.0f)
	{
		v2_move_towards(dst, angle, speed);
	}
	else
	{
		float r = speed + (distance - speed);
		v2_move_towards(dst, angle, r);

		if(end_callback) (*end_callback)();
	}
}

void v2_move_towards_v2_r_cb(v2 *dst, v2 target, float r, float speed, void(*end_callback))
{
	float angle = v2_azimuth(*dst, target);
	v2 target_r;
	v2_add(&target_r, target, v2_parametric(angle, r));

	// NOTE: recalculate azimuth.	
	v2_move_towards_v2_cb(dst, target_r, speed, end_callback);
}

void v2_move_towards_v2_d(v2 *dst, v2 target, float speed, float *get_distance)
{
	float angle = v2_azimuth(*dst, target);
	float distance = v2_distance(*dst, target);
	
	*get_distance = distance;
	
	if(distance - speed > 0.0f)
	{
		v2_move_towards(dst, angle, speed);
	}
	else
	{
		float r = speed + (distance - speed);
		v2_move_towards(dst, angle, r);
	}
}
void v2_move_towards_v2_r_d(v2 *dst, v2 target, float r, float speed, float *get_distance)
{
	float angle = v2_azimuth(*dst, target);
	v2 target_r;
	v2_add(&target_r, target, v2_parametric(angle, r));

	// NOTE: recalculate azimuth.	
	v2_move_towards_v2_d(dst, target_r, speed, get_distance);
}

void v2i_add(v2i *dst, v2i a, v2i b)
{
	dst->x = a.x + b.x;
	dst->y = a.y + b.y;
}

void motion_sine(float *dst_param, float origin_param, float magnitude, float period, float dtime)
{
	// y = a sin omega time
	// omega = 2PI f
	// a = magnitude
	// f = period
	*dst_param = magnitude * (float)(sin(2 * PI * period * dtime)) + origin_param;
}

float motion_tween_linear(float b, float c, float t, float d)
{
	return c * t / d + b;
}

void motion_tween(float *dst_param, float start_tween, float target_tween, float tween_time, float dtime, float(*tween_type)(float, float, float, float))
{
		if(dtime < tween_time)
		{				
				*dst_param = (*tween_type)(start_tween, (target_tween - start_tween), dtime, tween_time);
		}		
}

void run_timer(Timer *timer)
{
		timer->ticks = SDL_GetTicks() - timer->start_ticks;
}

void reset_timer(Timer *timer)
{
	timer->start_ticks = SDL_GetTicks();
	timer->ticks = 0.0f;
}

Circle* create_circle(v2 position, float radius)
{
	Circle *circle = malloc(sizeof(Circle));
	circle->position = position;
	circle->radius = radius;
	return circle;
}

bool is_circle_intersect(Circle *circle_a, Circle *circle_b)
{	
	float distance = v2_distance(circle_a->position, circle_b->position);
	float radius = circle_a->radius + circle_b->radius;
	bool intersect = distance < radius;
	return intersect;
}

bool is_circle_intersect_v2(Circle *circle, v2 point)
{	
	float distance = v2_distance(circle->position, point);
	bool intersect = distance < circle->radius;
	return intersect;

}