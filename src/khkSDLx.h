/*
License under zlib license
Copyright (C) 2025 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<stdbool.h>
#include<math.h>

#if defined _WIN32
#define SDL_MAIN_HANDLED
#endif
#include<SDL3/SDL.h>
#include<SDL3_image/SDL_image.h>

typedef SDL_Texture Texture;
typedef SDL_Surface Surface;

#define KBYTES 1024

#define SKYBLUE (SDL_Color){102, 191, 255, 255}
#define GREEN (SDL_Color){0, 228, 48, 255}
#define RED (SDL_Color){230, 41, 55, 255}
#define BLACK (SDL_Color){0, 0, 0, 255}
#define WHITE (SDL_Color){255, 255, 255, 255}

#ifndef BASE_MATH
#define BASE_MATH
#define PI 3.14159265359

#ifdef __cplusplus
extern "C"
{
#endif

double to_degree(float radian);
double to_radian(float degree);
int get_digit(int num); 

struct v2
{
	float x;
	float y;
};
typedef struct v2 v2;

#define v2_0 (v2){0.0f, 0.0f}
#define print_v2(val) printf("x:%f, y:%f\n", val.x, val.y)

void v2_sub(v2 *dst, v2 a, v2 b);
void v2_add(v2 *dst, v2 a, v2 b);
float v2_magnitude(v2);
float v2_distance(v2 a, v2 b);
float v2_azimuth(v2 a, v2 b);
v2 v2_parametric(float degree, float r);
void v2_move_towards(v2 *dst, float degree, float speed);
void v2_move_towards_v2(v2 *dst, v2 target, float speed);
void v2_move_towards_v2_r(v2 *dst, v2 target, float r, float speed);
void v2_move_towards_v2_d(v2 *dst, v2 target, float speed, float *get_distance);
void v2_move_towards_v2_r_d(v2 *dst, v2 target, float r, float speed, float *get_distance);
void v2_move_towards_v2_cb(v2 *dst, v2 target, float speed, void(*end_callback)());
void v2_move_towards_v2_r_cb(v2 *dst, v2 target, float r, float speed, void(*end_callback));

struct v2i
{
	int x;
	int y;
};
typedef struct v2i v2i;

#define v2i_0 (v2i){0, 0}
#define print_v2i(val) printf("x:%d, y:%d\n", val.x, val.y)

v2 to_v2(v2i v);
void v2i_add(v2i *dst, v2i a, v2i b);

struct Transform
{
	v2 position;
	v2 scale;
	double angle;
};
typedef struct Transform Transform;
#define transform_0 (Transform){(v2){0.0f, 0.0f}, (v2){1.0f, 1.0f}, 0.0f}

void motion_sine(float *dst_param, float origin_param, float magnitude, float period, float dtime);

// Robert Penner tween
float motion_tween_linear(float b, float c, float t, float d);
void motion_tween(float *dst_param, float start_tween, float target_tween, float tween_time, float dtime, float(*tween_type)(float, float, float, float));

struct Timer
{
		float start_ticks;
		float ticks;
};
typedef struct Timer Timer;
#define timer_0 (Timer){0.0f, 0.0f}

void run_timer(Timer *timer);
void reset_timer(Timer *timer);

struct Circle
{
	v2 position;
	float radius;
};
typedef struct Circle Circle;

Circle* create_circle(v2 position, float radius);
bool is_circle_intersect(Circle *circle_a, Circle *circle_b);
bool is_circle_intersect_v2(Circle *circle, v2 point);

#endif // BASE_MATH

#ifndef BASE_LIST
#define BASE_LIST

struct List
{
	void **t;
	size_t size;
};
typedef struct List List;

void create_list(List *list);
void list_push_back(List *list, void *element);
void list_pop_back(List *list);
void erase_list_element(List* list, int index);
void erase_list_element_element(List* list, void *element);
void swap_list_element(List *list, int index_a, int index_b);
void clear_list(List *list);

#endif // BASE_LIST

#ifndef BASE_NODE
#define BASE_NODE

struct Node;
typedef struct Node Node;

struct Node{
	const char *name;
	const char *tag;
	Transform transform;
	Transform rel_transform;
	Node *parent;
	Node **child;
	size_t child_size;
	// List components;
	void *data;
};

Node* create_node(const char *name, const char *tag);
void node_add_child(Node *parent, Node *child);
void node_pop_child(Node *parent);
void node_remove_child(Node *parent, int index);
void node_remove_child_node(Node *parent, Node *child);
void node_free_child(Node *parent, int index);
void node_free_child_node(Node *parent, Node *child);
void node_swap_child_index(Node *parent, int index_i, int index_j);
void node_set_position(Node *node, v2 position);
void node_set_position_x(Node *node, float x);
void node_set_position_y(Node *node, float y);
void node_set_rel_position(Node *node, v2 rel_position);
void node_set_rel_position_x(Node *node, float rel_x);
void node_set_rel_position_y(Node *node, float rel_y);
void node_move_towards(Node *node, float degree, float speed);
void node_move_towards_v2(Node *node, v2 target, float speed);
void node_move_towards_v2_r(Node *node, v2 target, float r, float speed);
void node_move_towards_v2_d(Node *node, v2 target, float speed, float *get_distance);
void node_move_towards_v2_r_d(Node *node, v2 target, float r, float speed, float *get_distance);
void node_move_towards_v2_cb(Node *node, v2 target, float speed, void(*end_callback)());
void node_move_towards_v2_r_cb(Node *node, v2 target, float r, float speed, void(*end_callback)());
void node_move_towards_node(Node *node, Node *target_node, float speed);
int get_child_index(Node *parent, Node *child);

#endif // BASE_NODE

#ifndef BASE_APP
#define BASE_APP

struct Sys{
		int target_fps;
		float dt;
		float ticks;
};
typedef struct Sys Sys;
extern Sys sys;

struct App;
typedef struct App App;

struct App
{
	int window_width;
	int window_height;
	v2i mouse_position;
	bool is_running;

	SDL_Window *window;
	SDL_Renderer *renderer;

	void(*on_init)(App*);
	void(*on_update)(App*);
	void(*on_render)(App*);
	void(*on_key_up)(App*, SDL_Keycode);
	void(*on_key_down)(App*, SDL_Keycode);
	void(*on_mouse_up)(App*, SDL_MouseButtonEvent);
	void(*on_mouse_down)(App*, SDL_MouseButtonEvent);
	void(*on_destroy)(App*);
};

App *create_app(const char *title, int window_width, int window_height, int target_fps);
void run_app(App *app);

#endif // BASE_APP

#ifndef BASE_FNT
#define BASE_FNT

struct Glyph
{
	int id;
	int x;
	int y;
	int width;
	int height;
	int xoffset;
	int yoffset;
	int xadvance;    
	int page;  
	int chnl;
};
typedef struct Glyph Glyph;

struct Fnt
{	
	int size; // font size
	int bold;
	int italic;
	int char_count;
	int kerning_count;
	char *image_src; // TODO: multiple pages/images
	Texture *texture;
	Glyph **glyph;	
};
typedef struct Fnt Fnt;

struct FntText
{
	const char *text;	
	Fnt *fnt;
	int *glyph_index_arr;
	int length;
	int width;
	int height;
	SDL_Point pivot;
};
typedef struct FntText FntText;

Fnt* load_fnt(SDL_Renderer *renderer, const char * src_file);
FntText* create_fnt_text(const char *text, Fnt *fnt);
void set_fnt_text(FntText *fnt_text, const char *text);
void render_fnt_text(SDL_Renderer *renderer, FntText *fnt_text, float pos_x, float pos_y);

#endif // BASE_FNT

#ifndef BASE_GRAPHICS
#define BASE_GRAPHICS

void render_background_color(SDL_Renderer *renderer, SDL_Color color);
Surface* load_surface(const char *src_file);
Texture* load_texture(SDL_Renderer *renderer, const char *src_file);
Texture* create_texture(SDL_Renderer *renderer, Surface *surface);
void render_texture(SDL_Renderer *renderer, Texture *texture, float pos_x, float pos_y);
void render_texture_v2(SDL_Renderer *renderer, Texture *texture, v2 position);
void render_texture_ex(SDL_Renderer *renderer, Texture *texture, float pos_x, float pos_y, double angle, SDL_FPoint *center, SDL_FlipMode flip);
void render_texture_ex_v2(SDL_Renderer *renderer, Texture *texture, v2 position, double angle, SDL_FPoint *center, SDL_FlipMode flip);
void render_sub_texture(SDL_Renderer *renderer, Texture *texture, SDL_Rect *src_rect, float pos_x, float pos_y);

// SDL_SetTextureAlphaMod(texture: SDL_Texture*, alpha: Uint8);

struct Sprite
{
	Texture *texture;
	float width;
	float height;
	SDL_FPoint pivot;
	SDL_FlipMode flip;
};
typedef struct Sprite Sprite;

Sprite *create_sprite(SDL_Renderer *renderer, Texture *texture);
Sprite *load_sprite(SDL_Renderer *renderer, const char *src_file);
void render_sprite(SDL_Renderer *renderer, Sprite *sprite, float pos_x, float pos_y);
void render_sprite_v2(SDL_Renderer *renderer, Sprite *sprite, v2 position, v2 scale, double angle);
void render_sprite_transform(SDL_Renderer *renderer, Sprite *sprite, Transform *transform);
void render_sub_sprite(SDL_Renderer *renderer, Sprite *sprite, float pos_x, float pos_y, SDL_FRect *src_rect);
void render_sub_sprite_v2(SDL_Renderer *renderer, Sprite *sprite, v2 position, v2 scale, double angle, SDL_FRect *src_rect, SDL_FPoint *pivot);
void render_sub_transform(SDL_Renderer *renderer, Sprite *sprite, Transform *transform, SDL_FRect *src_rect);
void destroy_sprite(Sprite *sprite);

struct SpriteAnimator
{
	Sprite *sprite;
	SDL_FPoint pivot;
	int target_fps;
	int curr_frame_index;

	struct
	{
		v2i size;
		v2i grid_size;
		SDL_FRect src_rect;
		float dt;
		float next_frame_ms;
	}frame;
};
typedef struct SpriteAnimator SpriteAnimator;

SpriteAnimator* create_sprite_animator(Sprite *sprite, v2i frame_size, int target_fps);
void run_sprite_animator(SpriteAnimator *sprite_animator, int *frames, int frame_length);
void render_sprite_animator(SDL_Renderer *renderer, SpriteAnimator *sprite_animator, float pos_x, float pos_y);
void render_sprite_animator_v2(SDL_Renderer *renderer, SpriteAnimator *sprite_animator, v2 position, v2 scale, double angle);
void render_sprite_animator_transform(SDL_Renderer *renderer, SpriteAnimator *sprite_animator, Transform *transform);

// NOTE: since I don't need anti aliased circle right now, 
// so I use this simple midpoint circle algorithm to avoid adding dependency by using 
// third party library like SDL2_gfx.
void render_circle(SDL_Renderer *renderer, Circle *circle, SDL_Color color, int filled);

#endif // BASE_GRAPHICS

#ifdef __cplusplus
}
#endif
