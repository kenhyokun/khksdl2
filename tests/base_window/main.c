/*
License under zlib license
Copyright (C) 2024 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include<khkSDLx.h>

void init(App *app)
{
}

void update(App *app)
{	
}

void render(App *app)
{
	SDL_SetRenderDrawColor(app->renderer, SKYBLUE.r, SKYBLUE.g, SKYBLUE.b, SKYBLUE.a);
	SDL_RenderFillRect(app->renderer, NULL);	
}

void key_up(App *app, SDL_Keycode key_code)
{
}

void key_down(App *app, SDL_Keycode key_code)
{
}

void mouse_up(App *app, SDL_MouseButtonEvent mouse)
{
}

void mouse_down(App *app, SDL_MouseButtonEvent mouse)
{
}

void destroy(App *app)
{
}

int main()
{
	App *app = create_app("SDL2 window", 400, 400, 60); 
	app->on_init = &init;
	app->on_update = &update;
	app->on_render = &render;
	app->on_key_up = &key_up;
	app->on_key_down = &key_down;
	app->on_mouse_up = &mouse_up;
	app->on_mouse_down = &mouse_down;
	app->on_destroy = &destroy;
	run_app(app);

	return 0;
}


