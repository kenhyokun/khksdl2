/*
License under zlib license
Copyright (C) 2024 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include<khkSDLx.h>

SDL_Rect dst_rect;
v2 position;

Texture *tile_texture;
Sprite *lilwitch;
SpriteAnimator *lilwitch_animator;
int anim_right[] = {0,1,2,3};
int anim_left[] = {4,5,6,7};
int anim_state = 0;
v2 lilwitch_position = {48.0f, 48.0f};

void init(App *app)
{
	position = (v2){48.0f, 48.0f};
	dst_rect = (SDL_Rect){100, 100, position.x, position.y};

	tile_texture = load_texture(app->renderer, "./resources/images/tile_bg.png");
	lilwitch = load_sprite(app->renderer, "./resources/images/lilwitch.png");
	lilwitch_animator = create_sprite_animator(lilwitch, (v2i){78, 87}, 3);
}

float move_speed = 3.33f;
float move_direction = 0.0f;
bool is_a_pressed = false;
bool is_d_pressed = false;
bool is_w_pressed = false;
bool is_s_pressed = false;
void update(App *app)
{
	switch(anim_state)
	{
		case 0:
			run_sprite_animator(lilwitch_animator, anim_left, 4);
			break;
		case 1:
			run_sprite_animator(lilwitch_animator, anim_right, 4);
			break;
	}

	if(position.y < app->window_height)
		position.y += 1.23f;
	else
		position.y = 0.0f;
	
	v2i render_position = get_screen_position(position.x, position.y);
	dst_rect.x = render_position.x;
	dst_rect.y = render_position.y;

	if(is_a_pressed)
	{
		anim_state = 0;
		move_direction = 270.0f;
	}
	if(is_d_pressed)
	{
		anim_state = 1;
		move_direction = 90.0f;
	}
	if(is_w_pressed)
	{
		move_direction = 0.0f;
	}
	if(is_s_pressed)
	{
		move_direction = 180.0f;
	}
	
	if(is_a_pressed || is_d_pressed || is_w_pressed || is_s_pressed)
	{
		v2_move_towards(&lilwitch_position, move_direction, move_speed);
	}
}

void render(App *app)
{
	SDL_SetRenderDrawColor(app->renderer, SKYBLUE.r, SKYBLUE.g, SKYBLUE.b, SKYBLUE.a);
	SDL_RenderFillRect(app->renderer, NULL);

	// NOTE: the width must be odd number to get checker thingy... 
	const int checker_width = 15;
	for(int i = 0; i < checker_width; ++i)
	{
		for(int j = 0; j < checker_width; ++j)
		{
			int k = i * checker_width + j;
			int curr_index = 0;

			if(k % 2 != 0)
			{
				curr_index = 1;
			}

			SDL_Rect src_rect = (SDL_Rect){curr_index * 48, 0, 48, 48};
			render_sub_texture(app->renderer, tile_texture, &src_rect, 0.0f + (j * 48), 0.0f + (i * 48));
		}
	}

	SDL_SetRenderDrawColor(app->renderer, 0, 0, 255, 255);
	SDL_RenderFillRect(app->renderer, &dst_rect);

	render_sprite(app->renderer, lilwitch, app->window_width / 2, app->window_height / 2);

	SDL_Rect sub_src_rect = {2 * 78, 0 * 87, 78, 87};
	SDL_Point pivot = {0, 0};
	render_sub_sprite_v2(app->renderer, lilwitch, (v2){app->window_width / 2, app->window_height / 2}, (v2){1.0f, 1.0f}, 0.0f, &sub_src_rect, &pivot);

	render_sprite_animator(app->renderer, lilwitch_animator, lilwitch_position.x, lilwitch_position.y);
}

void key_up(App *app, SDL_Keycode key_code)
{
	if(key_code == SDLK_a)
	{
		is_a_pressed = false;
	}

	if(key_code == SDLK_d)
	{
		is_d_pressed = false;
	}

	if(key_code == SDLK_w)
	{
		is_w_pressed = false;
	}

	if(key_code == SDLK_s)
	{
		is_s_pressed = false;
	}
}

void key_down(App *app, SDL_Keycode key_code)
{
	if(key_code == SDLK_a)
	{
		is_a_pressed = true;
	}

	if(key_code == SDLK_d)
	{
		is_d_pressed = true;
	}

	if(key_code == SDLK_w)
	{
		is_w_pressed = true;
	}

	if(key_code == SDLK_s)
	{
		is_s_pressed = true;
	}
}

void mouse_up(App *app, SDL_MouseButtonEvent mouse)
{
}

void mouse_down(App *app, SDL_MouseButtonEvent mouse)
{
	if(mouse.button == SDL_BUTTON_LEFT){
		printf("mouse button left down...\n");
	}

	if(mouse.clicks == 2){
		printf("mouse button left double clicks\n");
	}
}

void destroy(App *app)
{
	SDL_DestroyTexture(tile_texture);	
	destroy_sprite(lilwitch);
}

int main()
{
	App *app = create_app("SDL2 window", 400, 400, 60); 
	app->on_init = &init;
	app->on_update = &update;
	app->on_render = &render;
	app->on_key_up = &key_up;
	app->on_key_down = &key_down;
	app->on_mouse_up = &mouse_up;
	app->on_mouse_down = &mouse_down;
	app->on_destroy = &destroy;
	run_app(app);

	return 0;
}


